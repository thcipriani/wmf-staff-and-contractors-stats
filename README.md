# WMF Staff and Contractors Stats

Descriptive statistics on tenure


```python
import datetime
import subprocess
import dateparser

REPO = 'submodules/wmf-staff-and-contractors.git'

def extract_date(blame_line: str):
    blame_line = blame_line.split(' ')
    return dateparser.parse('{}T{}{}'.format(blame_line[3], blame_line[4], blame_line[5]))

def git_start_dates(rev='HEAD'):
    blame = subprocess.check_output([
        '/usr/bin/git', '-C', REPO, 'blame', rev, 'README'
    ]).decode('utf8').splitlines()
    print(blame[0])
    # skip the first two lines since they're the count and "====="
    return [extract_date(x) for x in blame[2:]]

def git_commit_date(rev='HEAD'):
    return dateparser.parse(subprocess.check_output([
        '/usr/bin/git', '-C', REPO, 'log', '-1', rev, '--format=%ad'
    ]).decode('utf8').strip())

def get_time_deltas(rev='HEAD', commit_date=None):
    if commit_date is None:
        commit_date = git_commit_date(rev)
    start_dates = git_start_dates(rev)
    return [commit_date - start_date for start_date in start_dates]
```


```python
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

df = pd.DataFrame(get_time_deltas('HEAD'), columns = ['time'])
df.describe()
```

    f9aa035f (Tyler Cipriani 2021-10-27 03:00:03 -0600   1) 489 WMF folks





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>time</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>489</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>1315 days 02:25:12.901840656</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1143 days 23:14:14.433464240</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0 days 00:00:00</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>313 days 01:59:58</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>992 days 01:59:56</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>2248 days 03:00:03</td>
    </tr>
    <tr>
      <th>max</th>
      <td>4074 days 03:00:03</td>
    </tr>
  </tbody>
</table>
</div>




```python
import numpy as np
import time

tenure_stats = {}

start = time.time()
total_commits = int(subprocess.check_output(['/usr/bin/git', '-C', REPO, 'rev-list', 'HEAD', '--count']).decode('utf8'))

for offset in range(0, total_commits):
# for offset in range(0, 10):
    rev = f'HEAD~{offset}'
    d = git_commit_date(rev)
    l = get_time_deltas(rev, d)
    tenure_stats[d.strftime('%Y-%m-%d')] = [
        len(l),
        np.mean(l),
        np.median(l),
        np.percentile(l, 25),
        np.percentile(l, 75),
        np.percentile(l, 95),
    ]
    
print('{} seconds'.format(time.time() - start))
```

    f9aa035f (Tyler Cipriani 2021-10-27 03:00:03 -0600   1) 489 WMF folks
    d3855f36 (Tyler Cipriani 2021-10-19 03:00:03 -0600   1) 488 WMF folks
    339029ce (Tyler Cipriani 2021-09-28 11:49:06 -0600   1) 491 WMF folks
    85c5fab2 (Tyler Cipriani 2021-09-20 14:36:48 -0600   1) 482 WMF folks
    f0b8f0ab (Tyler Cipriani 2021-08-06 00:00:03 -0600   1) 447 WMF folks
    15cbeab2 (Tyler Cipriani 2021-08-04 00:00:04 -0600   1) 452 WMF folks
    3113890d (Tyler Cipriani 2021-07-16 00:00:06 -0600   1) 454 WMF folks
    2a8370e3 (Tyler Cipriani 2021-06-29 00:00:06 -0600   1) 456 WMF folks
    a2ab0f10 (Tyler Cipriani 2021-06-18 00:00:06 -0600   1) 458 WMF folks
    a2ab0f10 (Tyler Cipriani 2021-06-18 00:00:06 -0600   1) 458 WMF folks
    22ccb6db (Tyler Cipriani 2021-06-15 00:00:04 -0600   1) 443 WMF folks
    d9049114 (Tyler Cipriani 2021-05-21 00:00:05 -0600   1) 445 WMF folks
    d97b2130 (Tyler Cipriani 2021-04-27 00:00:04 -0600   1) 441 WMF folks
    d75e61c5 (Tyler Cipriani 2021-04-16 00:00:05 -0600   1) 438 WMF folks
    76ddb5af (Tyler Cipriani 2021-04-08 00:00:05 -0600   1) 437 WMF folks
    4e92db81 (Tyler Cipriani 2021-04-01 00:00:05 -0600   1) 438 WMF folks
    b228af34 (Tyler Cipriani 2021-03-09 00:00:04 -0700   1) 425 WMF folks
    a3c94cf4 (Tyler Cipriani 2021-02-25 00:00:05 -0700   1) 422 WMF folks
    3ce4d863 (Tyler Cipriani 2021-02-19 00:00:05 -0700   1) 418 WMF folks
    9e671e73 (Tyler Cipriani 2021-02-09 00:00:04 -0700   1) 419 WMF folks
    70aa1047 (Tyler Cipriani 2021-01-30 00:00:05 -0700   1) 415 WMF folks
    43376067 (Tyler Cipriani 2021-01-29 00:00:05 -0700   1) 416 WMF folks
    df4ba3d8 (Tyler Cipriani 2021-01-07 10:13:51 -0700   1) 414 WMF folks
    3bfb66cb (Tyler Cipriani 2020-12-18 00:00:05 -0700   1) 416 WMF folks
    471cc96b (Tyler Cipriani 2020-12-02 00:00:05 -0700   1) 415 WMF folks
    abca9993 (Tyler Cipriani 2020-11-10 00:00:05 -0700   1) 410 WMF folks
    cdb5a7ac (Tyler Cipriani 2020-10-30 00:00:06 -0600   1) 408 WMF folks
    e2352238 (Tyler Cipriani 2020-10-02 00:00:06 -0600   1) 409 WMF folks
    e2352238 (Tyler Cipriani 2020-10-02 00:00:06 -0600   1) 409 WMF folks
    f2fb242b (Tyler Cipriani 2020-10-01 00:00:04 -0600   1) 410 WMF folks
    f164d242 (Tyler Cipriani 2020-09-09 00:00:05 -0600   1) 416 WMF folks
    e5b19620 (Tyler Cipriani 2020-09-03 00:00:05 -0600   1) 415 WMF folks
    de0b7c54 (Tyler Cipriani 2020-08-26 00:00:04 -0600   1) 413 WMF folks
    9dbd6720 (Tyler Cipriani 2020-08-25 00:00:05 -0600   1) 414 WMF folks
    5931eb1d (Tyler Cipriani 2020-07-30 00:00:04 -0600   1) 422 WMF folks
    5931eb1d (Tyler Cipriani 2020-07-30 00:00:04 -0600   1) 422 WMF folks
    023d06ef (Tyler Cipriani 2020-07-25 00:00:05 -0600   1) 421 WMF folks
    7b7da875 (Tyler Cipriani 2020-07-22 00:00:06 -0600   1) 420 WMF folks
    fc988532 (Tyler Cipriani 2020-07-19 00:00:05 -0600   1) 421 WMF folks
    fc988532 (Tyler Cipriani 2020-07-19 00:00:05 -0600   1) 421 WMF folks
    bcd8e662 (Tyler Cipriani 2020-07-17 00:00:05 -0600   1) 422 WMF folks
    82bd3aba (Tyler Cipriani 2020-07-15 00:00:05 -0600   1) 421 WMF folks
    4217206c (Tyler Cipriani 2020-07-14 00:00:05 -0600   1) 418 WMF folks
    2c6101be (Tyler Cipriani 2020-07-02 00:00:04 -0600   1) 416 WMF folks
    834e1a6a (Tyler Cipriani 2020-06-19 00:00:05 -0600   1) 417 WMF folks
    6de7cfb6 (Tyler Cipriani 2020-06-16 00:00:04 -0600   1) 418 WMF folks
    66de4f77 (Tyler Cipriani 2020-05-09 00:00:05 -0600   1) 407 WMF folks
    de067195 (Tyler Cipriani 2020-05-06 00:00:04 -0600   1) 402 WMF folks
    006f2506 (Tyler Cipriani 2020-04-17 00:00:03 -0600   1) 401 WMF folks
    e5863a2c (Tyler Cipriani 2020-03-19 00:00:04 -0600   1) 396 WMF folks
    dd179aaa (Tyler Cipriani 2020-03-14 00:00:03 -0600   1) 395 WMF folks
    96afeeb7 (Tyler Cipriani 2020-03-13 00:00:05 -0600   1) 396 WMF folks
    53cfe14e (Tyler Cipriani 2020-03-11 00:00:03 -0600   1) 398 WMF folks
    fb9ae74a (Tyler Cipriani 2020-03-05 00:00:03 -0700   1) 397 WMF folks
    fa047f1e (Tyler Cipriani 2020-02-14 00:00:03 -0700   1) 399 WMF folks
    bf59c884 (Tyler Cipriani 2020-02-11 00:00:03 -0700   1) 397 WMF folks
    8adae5c7 (Tyler Cipriani 2020-02-08 00:00:03 -0700   1) 396 WMF folks
    b9bbb3e2 (Tyler Cipriani 2020-02-04 00:00:04 -0700   1) 392 WMF folks
    8587eacb (Tyler Cipriani 2020-01-25 00:00:04 -0700   1) 393 WMF folks
    58e2dc57 (Tyler Cipriani 2020-01-18 00:00:03 -0700   1) 394 WMF folks
    58e2dc57 (Tyler Cipriani 2020-01-18 00:00:03 -0700   1) 394 WMF folks
    26d5774d (Tyler Cipriani 2020-01-14 00:00:03 -0700   1) 393 WMF folks
    7c739cd0 (Tyler Cipriani 2020-01-07 00:00:03 -0700   1) 388 WMF folks
    f61933e3 (Tyler Cipriani 2019-12-19 00:00:03 -0700   1) 384 WMF folks
    7c78c182 (Tyler Cipriani 2019-12-13 00:00:03 -0700   1) 382 WMF folks
    4331e18c (Tyler Cipriani 2019-12-10 00:00:03 -0700   1) 383 WMF folks
    ab124bc2 (Tyler Cipriani 2019-11-27 00:00:03 -0700   1) 377 WMF folks
    3659f1b7 (Tyler Cipriani 2019-11-21 00:00:03 -0700   1) 378 WMF folks
    be482cf2 (Tyler Cipriani 2019-11-02 00:00:04 -0600   1) 382 WMF folks
    32447c54 (Tyler Cipriani 2019-10-24 00:00:03 -0600   1) 384 WMF folks
    c9c55a6a (Tyler Cipriani 2019-10-22 00:00:03 -0600   1) 383 WMF folks
    244d6ac8 (Tyler Cipriani 2019-10-16 00:00:03 -0600   1) 381 WMF folks
    cc1dfdb1 (Tyler Cipriani 2019-10-08 01:00:03 -0600   1) 379 WMF folks
    6b59b23d (Tyler Cipriani 2019-10-05 01:00:03 -0600   1) 376 WMF folks
    704d3b1b (Tyler Cipriani 2019-10-02 01:00:03 -0600   1) 375 WMF folks
    704d3b1b (Tyler Cipriani 2019-10-02 01:00:03 -0600   1) 375 WMF folks
    058bdc1e (Tyler Cipriani 2019-09-27 00:00:03 -0700   1) 373 WMF folks
    058bdc1e (Tyler Cipriani 2019-09-27 00:00:03 -0700   1) 373 WMF folks
    83ff4ba4 (Tyler Cipriani 2019-09-18 00:00:04 -0600   1) 372 WMF folks
    79c14265 (Tyler Cipriani 2019-09-13 00:00:03 -0600   1) 373 WMF folks
    276f5ebe (Tyler Cipriani 2019-09-10 00:00:03 -0600   1) 378 WMF folks
    b0b2872b (Tyler Cipriani 2019-09-07 00:00:04 -0600   1) 376 WMF folks
    e1e6840e (Tyler Cipriani 2019-09-06 00:00:03 -0600   1) 374 WMF folks
    699acfda (Tyler Cipriani 2019-08-29 00:00:04 -0600   1) 372 WMF folks
    699acfda (Tyler Cipriani 2019-08-29 00:00:04 -0600   1) 372 WMF folks
    aa41d4f2 (Tyler Cipriani 2019-08-23 00:00:04 -0600   1) 371 WMF folks
    2f811fc3 (Tyler Cipriani 2019-08-20 00:00:03 -0600   1) 370 WMF folks
    8318e0d6 (Tyler Cipriani 2019-08-17 00:00:03 -0600   1) 369 WMF folks
    6c8d144d (Tyler Cipriani 2019-08-09 00:00:04 -0600   1) 368 WMF folks
    6c8d144d (Tyler Cipriani 2019-08-09 00:00:04 -0600   1) 368 WMF folks
    d94adab6 (Tyler Cipriani 2019-08-03 00:00:03 -0600   1) 364 WMF folks
    11137d6f (Tyler Cipriani 2019-08-02 00:00:06 -0600   1) 360 WMF folks
    6ad489cb (Tyler Cipriani 2019-07-13 00:00:03 -0600   1) 363 WMF folks
    f7ae35e9 (Tyler Cipriani 2019-07-05 00:00:03 -0600   1) 361 WMF folks
    84533b96 (Tyler Cipriani 2019-07-04 00:00:04 -0600   1) 363 WMF folks
    266a1d7e (Tyler Cipriani 2019-06-27 00:00:05 -0600   1) 367 WMF folks
    37543c39 (Tyler Cipriani 2019-06-19 00:00:06 -0600   1) 366 WMF folks
    40f24ac9 (Tyler Cipriani 2019-06-18 00:00:05 -0600   1) 368 WMF folks
    cb316f47 (Tyler Cipriani 2019-06-12 00:00:06 -0600   1) 367 WMF folks
    f9fbbf09 (Tyler Cipriani 2019-05-21 00:00:03 -0600   1) 364 WMF folks
    f9fbbf09 (Tyler Cipriani 2019-05-21 00:00:03 -0600   1) 364 WMF folks
    2568a228 (Tyler Cipriani 2019-05-14 00:00:04 -0600   1) 360 WMF folks
    33e66cb9 (Tyler Cipriani 2019-04-30 00:00:03 -0600   1) 358 WMF folks
    33e66cb9 (Tyler Cipriani 2019-04-30 00:00:03 -0600   1) 358 WMF folks
    4cbbf2aa (Tyler Cipriani 2019-04-19 00:00:04 -0600   1) 356 WMF folks
    3ee88582 (Tyler Cipriani 2019-04-17 00:00:04 -0600   1) 357 WMF folks
    10686324 (Tyler Cipriani 2019-04-16 00:00:03 -0600   1) 358 WMF folks
    03979cd7 (Tyler Cipriani 2019-04-06 00:00:03 -0600   1) 356 WMF folks
    040bfc01 (Tyler Cipriani 2019-03-28 00:00:04 -0600   1) 351 WMF folks
    191add93 (Tyler Cipriani 2019-03-26 00:00:04 -0600   1) 349 WMF folks
    e11eab5f (Tyler Cipriani 2019-03-20 00:00:04 -0600   1) 351 WMF folks
    e4134e26 (Tyler Cipriani 2019-03-01 00:00:03 -0700   1) 353 WMF folks
    24b27a48 (Tyler Cipriani 2019-02-27 00:00:04 -0700   1) 356 WMF folks
    2302d268 (Tyler Cipriani 2019-02-20 00:00:04 -0700   1) 354 WMF folks
    2ca7a216 (Tyler Cipriani 2019-02-15 00:00:04 -0700   1) 352 WMF folks
    85f552a7 (Tyler Cipriani 2019-02-14 00:00:04 -0700   1) 353 WMF folks
    687485be (Tyler Cipriani 2019-02-08 00:00:07 -0700   1) 349 WMF folks
    ad5c1b23 (Tyler Cipriani 2019-02-06 00:00:05 -0700   1) 348 WMF folks
    ce0ee535 (Tyler Cipriani 2019-02-01 00:00:03 -0700   1) 349 WMF folks
    516039fe (Tyler Cipriani 2019-01-17 00:00:03 -0700   1) 347 WMF folks
    db138530 (Tyler Cipriani 2019-01-08 00:00:03 -0700   1) 348 WMF folks
    db138530 (Tyler Cipriani 2019-01-08 00:00:03 -0700   1) 348 WMF folks
    2c403c10 (Tyler Cipriani 2019-01-06 00:00:03 -0700   1) 346 WMF folks
    896c9a11 (Tyler Cipriani 2019-01-03 16:57:54 -0700   1) 347 WMF folks
    07f00660 (Tyler Cipriani 2018-12-18 11:52:58 -0700   1) 344 WMF folks
    4b59685a (Tyler Cipriani 2018-06-01 00:00:00 -0600   1) 311 WMF folks
    ce428175 (Tyler Cipriani 2018-05-01 00:00:00 -0600   1) 307 WMF folks
    9c102556 (Tyler Cipriani 2018-04-01 00:00:00 -0600   1) 309 WMF folks
    6d275ecd (Tyler Cipriani 2018-03-01 00:00:00 -0700   1) 305 WMF folks
    e31d9611 (Tyler Cipriani 2018-02-01 00:00:00 -0700   1) 303 WMF folks
    bee8a2d4 (Tyler Cipriani 2017-12-01 00:00:00 -0700   1) 300 WMF folks
    bee8a2d4 (Tyler Cipriani 2017-12-01 00:00:00 -0700   1) 300 WMF folks
    39f5f848 (Tyler Cipriani 2017-11-01 00:00:00 -0600   1) 298 WMF folks
    3bdd859d (Tyler Cipriani 2017-10-01 00:00:00 -0600   1) 297 WMF folks
    4f8f0700 (Tyler Cipriani 2017-08-01 00:00:00 -0600   1) 294 WMF folks
    4f8f0700 (Tyler Cipriani 2017-08-01 00:00:00 -0600   1) 294 WMF folks
    9cb75475 (Tyler Cipriani 2017-07-01 00:00:00 -0600   1) 289 WMF folks
    1628d6c0 (Tyler Cipriani 2017-06-01 00:00:00 -0600   1) 284 WMF folks
    9a27d64d (Tyler Cipriani 2017-04-01 00:00:00 -0600   1) 283 WMF folks
    9a27d64d (Tyler Cipriani 2017-04-01 00:00:00 -0600   1) 283 WMF folks
    3e409384 (Tyler Cipriani 2017-02-01 00:00:00 -0700   1) 286 WMF folks
    3e409384 (Tyler Cipriani 2017-02-01 00:00:00 -0700   1) 286 WMF folks
    c73b7303 (Tyler Cipriani 2017-01-01 00:00:00 -0700   1) 289 WMF folks
    5f794f41 (Tyler Cipriani 2016-12-01 00:00:00 -0700   1) 286 WMF folks
    61a0f849 (Tyler Cipriani 2016-11-01 00:00:00 -0600   1) 288 WMF folks
    fdad011e (Tyler Cipriani 2016-10-01 00:00:00 -0600   1) 279 WMF folks
    c6d1eccf (Tyler Cipriani 2016-09-01 00:00:00 -0600   1) 274 WMF folks
    2986cff2 (Tyler Cipriani 2016-07-01 00:00:00 -0600   1) 271 WMF folks
    2986cff2 (Tyler Cipriani 2016-07-01 00:00:00 -0600   1) 271 WMF folks
    5104b0dc (Tyler Cipriani 2016-06-01 00:00:00 -0600   1) 265 WMF folks
    4ef5d906 (Tyler Cipriani 2016-05-01 00:00:00 -0600   1) 278 WMF folks
    66f2ef2e (Tyler Cipriani 2016-04-01 00:00:00 -0600   1) 280 WMF folks
    5fd81978 (Tyler Cipriani 2016-03-01 00:00:00 -0700   1) 288 WMF folks
    9eba497e (Tyler Cipriani 2016-02-01 00:00:00 -0700   1) 290 WMF folks
    39fd60ef (Tyler Cipriani 2016-01-01 00:00:00 -0700   1) 286 WMF folks
    9cd561ae (Tyler Cipriani 2015-12-01 00:00:00 -0700   1) 285 WMF folks
    19bd6ecf (Tyler Cipriani 2015-11-01 00:00:00 -0600   1) 283 WMF folks
    0de00f45 (Tyler Cipriani 2015-10-01 00:00:00 -0600   1) 281 WMF folks
    a66ad7ba (Tyler Cipriani 2015-09-01 00:00:00 -0600   1) 280 WMF folks
    6af56bb1 (Tyler Cipriani 2015-07-01 00:00:00 -0600   1) 276 WMF folks
    6af56bb1 (Tyler Cipriani 2015-07-01 00:00:00 -0600   1) 276 WMF folks
    71e02706 (Tyler Cipriani 2015-06-01 00:00:00 -0600   1) 267 WMF folks
    7be62e5c (Tyler Cipriani 2015-05-01 00:00:00 -0600   1) 261 WMF folks
    996bb91b (Tyler Cipriani 2015-04-01 00:00:00 -0600   1) 259 WMF folks
    0d148654 (Tyler Cipriani 2015-03-01 00:00:00 -0700   1) 250 WMF folks
    727294c4 (Tyler Cipriani 2015-02-01 00:00:00 -0700   1) 248 WMF folks
    f7341ad8 (Tyler Cipriani 2014-12-01 00:00:00 -0700   1) 241 WMF folks
    f7341ad8 (Tyler Cipriani 2014-12-01 00:00:00 -0700   1) 241 WMF folks
    eabe8e31 (Tyler Cipriani 2014-11-01 00:00:00 -0600   1) 231 WMF folks
    178f9a0a (Tyler Cipriani 2014-10-01 00:00:00 -0600   1) 223 WMF folks
    8c2e49d0 (Tyler Cipriani 2014-09-01 00:00:00 -0600   1) 215 WMF folks
    a6ded030 (Tyler Cipriani 2014-08-01 00:00:00 -0600   1) 217 WMF folks
    9f6905bd (Tyler Cipriani 2014-06-01 00:00:00 -0600   1) 212 WMF folks
    9f6905bd (Tyler Cipriani 2014-06-01 00:00:00 -0600   1) 212 WMF folks
    d418aa16 (Tyler Cipriani 2014-05-01 00:00:00 -0600   1) 210 WMF folks
    35a3f8bf (Tyler Cipriani 2014-04-01 00:00:00 -0600   1) 205 WMF folks
    6622626a (Tyler Cipriani 2014-03-01 00:00:00 -0700   1) 202 WMF folks
    271be9b0 (Tyler Cipriani 2014-02-01 00:00:00 -0700   1) 201 WMF folks
    54e64b2b (Tyler Cipriani 2014-01-01 00:00:00 -0700   1) 197 WMF folks
    9256f17b (Tyler Cipriani 2013-12-01 00:00:00 -0700   1) 193 WMF folks
    5cc2a7ca (Tyler Cipriani 2013-11-01 00:00:00 -0600   1) 192 WMF folks
    14d446d4 (Tyler Cipriani 2013-10-01 00:00:00 -0600   1) 187 WMF folks
    2f6ff4fe (Tyler Cipriani 2013-09-01 00:00:00 -0600   1) 181 WMF folks
    f6dba153 (Tyler Cipriani 2013-08-01 00:00:00 -0600   1) 182 WMF folks
    52d828eb (Tyler Cipriani 2013-07-01 00:00:00 -0600   1) 175 WMF folks
    7b8f104f (Tyler Cipriani 2013-06-01 00:00:00 -0600   1) 165 WMF folks
    b5b2d837 (Tyler Cipriani 2013-05-01 00:00:00 -0600   1) 161 WMF folks
    63f92487 (Tyler Cipriani 2013-04-01 00:00:00 -0600   1) 163 WMF folks
    905ed977 (Tyler Cipriani 2013-03-01 00:00:00 -0700   1) 154 WMF folks
    40ac0473 (Tyler Cipriani 2013-02-01 00:00:00 -0700   1) 148 WMF folks
    47a76c5c (Tyler Cipriani 2013-01-01 00:00:00 -0700   1) 146 WMF folks
    6aa8a251 (Tyler Cipriani 2012-12-01 00:00:00 -0700   1) 149 WMF folks
    5155ed79 (Tyler Cipriani 2012-11-01 00:00:00 -0600   1) 145 WMF folks
    2b3eccea (Tyler Cipriani 2012-10-01 00:00:00 -0600   1) 143 WMF folks
    de984514 (Tyler Cipriani 2012-09-01 00:00:00 -0600   1) 139 WMF folks
    28d0671e (Tyler Cipriani 2012-08-01 00:00:00 -0600   1) 149 WMF folks
    8fbc61ef (Tyler Cipriani 2012-07-01 00:00:00 -0600   1) 148 WMF folks
    5928d99f (Tyler Cipriani 2012-06-01 00:00:00 -0600   1) 143 WMF folks
    9a4d2799 (Tyler Cipriani 2012-05-01 00:00:00 -0600   1) 131 WMF folks
    05fded20 (Tyler Cipriani 2012-04-01 00:00:00 -0600   1) 128 WMF folks
    c8910f41 (Tyler Cipriani 2012-03-01 00:00:00 -0700   1) 107 WMF folks
    3e89d5a0 (Tyler Cipriani 2012-02-01 00:00:00 -0700   1) 106 WMF folks
    e6c27525 (Tyler Cipriani 2012-01-01 00:00:00 -0700   1) 103 WMF folks
    0b1859b0 (Tyler Cipriani 2011-12-01 00:00:00 -0700   1) 100 WMF folks
    411be208 (Tyler Cipriani 2011-11-01 00:00:00 -0600  1) 97 WMF folks
    be45d5fc (Tyler Cipriani 2011-10-01 00:00:00 -0600  1) 90 WMF folks
    54707115 (Tyler Cipriani 2011-09-01 00:00:00 -0600  1) 78 WMF folks
    2c7b902a (Tyler Cipriani 2011-08-01 00:00:00 -0600  1) 76 WMF folks
    3c3df7bc (Tyler Cipriani 2011-07-01 00:00:00 -0600  1) 75 WMF folks
    d0f64c4f (Tyler Cipriani 2011-06-01 00:00:00 -0600  1) 65 WMF folks
    c8fe0a68 (Tyler Cipriani 2011-05-01 00:00:00 -0600  1) 64 WMF folks
    7ca1bb98 (Tyler Cipriani 2011-04-01 00:00:00 -0600  1) 63 WMF folks
    e30e5b5e (Tyler Cipriani 2011-03-01 00:00:00 -0700  1) 56 WMF folks
    9008fd13 (Tyler Cipriani 2011-02-01 00:00:00 -0700  1) 57 WMF folks
    f43cf7fa (Tyler Cipriani 2010-12-01 00:00:00 -0700  1) 58 WMF folks
    f43cf7fa (Tyler Cipriani 2010-12-01 00:00:00 -0700  1) 58 WMF folks
    3af432ee (Tyler Cipriani 2010-11-01 00:00:00 -0600  1) 56 WMF folks
    fa78d63f (Tyler Cipriani 2010-10-01 00:00:00 -0600  1) 58 WMF folks
    ^b9b3a56 (Tyler Cipriani 2010-09-01 00:00:00 -0600  1) 49 WMF folks
    148.3470549583435 seconds



```python
df = pd.DataFrame.from_dict(tenure_stats, orient='index', columns=['count', 'mean', 'median', '25', '75', '95'])
df.index = pd.to_datetime(df.index)
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>count</th>
      <th>mean</th>
      <th>median</th>
      <th>25</th>
      <th>75</th>
      <th>95</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2021-10-27</th>
      <td>489</td>
      <td>1315 days 02:25:12.901840</td>
      <td>992 days 01:59:56</td>
      <td>313 days 01:59:58</td>
      <td>2248 days 03:00:03</td>
      <td>3496 days 03:00:03</td>
    </tr>
    <tr>
      <th>2021-10-19</th>
      <td>488</td>
      <td>1309 days 19:05:50.428279</td>
      <td>995 days 01:59:58</td>
      <td>317 days 01:59:58</td>
      <td>2240 days 03:00:03</td>
      <td>3488 days 03:00:03</td>
    </tr>
    <tr>
      <th>2021-09-28</th>
      <td>491</td>
      <td>1297 days 18:25:46.289206</td>
      <td>985 days 10:49:03</td>
      <td>300 days 10:49:01</td>
      <td>2219 days 11:49:06</td>
      <td>3467 days 11:49:06</td>
    </tr>
    <tr>
      <th>2021-09-20</th>
      <td>482</td>
      <td>1329 days 14:41:12.147303</td>
      <td>977 days 13:36:45</td>
      <td>382 days 14:36:43</td>
      <td>2242 days 14:36:48</td>
      <td>3459 days 14:36:48</td>
    </tr>
    <tr>
      <th>2021-08-06</th>
      <td>447</td>
      <td>1425 days 06:21:04.002237</td>
      <td>961 days 11:07:05</td>
      <td>476 days 00:00:00</td>
      <td>2258 days 00:00:03</td>
      <td>3414 days 00:00:03</td>
    </tr>
  </tbody>
</table>
</div>



 # Median tenure at WMF over time


```python
df['median'].head()
```




    2021-10-27   992 days 01:59:56
    2021-10-19   995 days 01:59:58
    2021-09-28   985 days 10:49:03
    2021-09-20   977 days 13:36:45
    2021-08-06   961 days 11:07:05
    Name: median, dtype: timedelta64[ns]




```python
fig, ax = plt.subplots(figsize=[20,8])
df['median'].astype('timedelta64[M]').plot.line(title='Median tenure at WMF (months)', ax=ax)
ax.set_ylabel('Months')
plt.show()
```


![png](README_files/README_7_0.png)



```python
fig, ax = plt.subplots(figsize=[20,8])
df['25'].astype('timedelta64[M]').plot.line(title='25-percentile tenure at WMF (months)', ax=ax)
ax.set_ylabel('Months')
plt.show()
```


![png](README_files/README_8_0.png)



```python

```
